package com.example.student.todolist;

import android.support.v4.app.Fragment;

public class MainActivity extends SingleFragmentActivity {


    @Override
    protected Fragment createFragment() {
        return new ToDoFragment();
    }

    @Override
    protected int getLayoutRedId() {
        return 0;
    }
}
